# **HOW TO RUN THIS CODE**

1. Run npm -i
2. Run locally using localhost port that has given in server.js
3. Here is the dotenv properties:

- CONNECTION_URL = 'mongodb+srv://hired:hired123@cluster0.hvtwp.mongodb.net/db_hired?authSource=admin&replicaSet=atlas-ol0u27-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true'

- TOKEN_SECRET = 'backendtime'


# Documentaion API

### Feature Homepage
```
All Jobs and Companies
GET     /api/homepage/

Search Job By Title
GET     /api/homepage/search
    
Popular Job Categories
GET     /api/homepage/jobsearch

Feature Job By POST
GET     /api/homepage/jobfeaturesearch
    
Top Companies
GET     /api/homepage/topcompaniessearch
```
### Feature Company
```
All Jobs and Companies
GET     /api/company/
    
Search Job By Name
GET     /api/company/search
    
Create Company
POST    /api/company/

Update Company
PUT     /api/company/:id
    
DELETE Company
DELETE  /api/company/:id
```