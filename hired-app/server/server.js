require("dotenv").config();

const express = require("express");
const cors = require("cors");

const PORT = process.env.PORT || 5000;
const app = express();

app.use(express.json()); //Enable req.body JSON type
app.use(express.urlencoded({ extended: false })); //Support urlencode body
app.use(cors());

//import router
const jobLevelRoutes = require("./src/routes/jobLevelRoutes");
const userRouter = require("./src/routes/userRoute");
const roleRouter = require("./src/routes/roleRoutes");
const loginRouter = require("./src/routes/loginRoutes");
const jobTypesRouter = require("./src/routes/jobTypesRoutes");
const salaryFrequencyRouter = require("./src/routes/salaryFrequencyRoutes");
const salaryRangeRouter = require("./src/routes/salaryRangeRoutes");
const jobPostingCategoryRouter = require("./src/routes/jobPostingCategoryRoutes");
const companyRoutes = require("./src/routes/companyRoutes");
const companySizeRouter = require("./src/routes/companySizeRoutes");
const industry_categories = require("./src/routes/industry_categoriesRoutes");
const homepages = require("./src/routes/homePageRoutes");
const applyJobs = require("./src/routes/applyJobRoutes");
// const myProfile = require("./src/routes/myProfileRoutes");

//JobDetails Router
const jobDetails = require("./src/routes/jobDetailsRoutes");
const getJobDetails = require("./src/routes/jobDetailsRoutes");
const updateJobDetails = require("./src/routes/jobDetailsRoutes");
const deleteJobDetails = require("./src/routes/jobDetailsRoutes");
const getJobList = require("./src/routes/jobDetailsRoutes");
const getSearchJobs = require("./src/routes/jobDetailsRoutes");

// importing middleware
const auth = require("../server/src/middleware/auth");
const { employee, candidate } = require("../server/src/middleware/auth");

//Routing API
app.use("/api/v1/jobLevel", jobLevelRoutes);
app.use("/api/user", userRouter);
app.use("/api/role", roleRouter);
app.use("/api/login", loginRouter);
app.use("/api/jobTypes", jobTypesRouter);
app.use("/api/salaryFrequency", salaryFrequencyRouter);
app.use("/api/salaryrange", salaryRangeRouter);
app.use("/api/jobPostingCategory", jobPostingCategoryRouter);
app.use("/api/company", companyRoutes);
app.use("/api/companySize", companySizeRouter);
app.use("/api/industry_categories", industry_categories);
app.use("/api/homepage", homepages);
app.use("/api/createJobDetail", jobDetails);
app.use("/api/getAllJobDetails", getJobDetails);
app.use("/api/updateJobDetails", updateJobDetails);
app.use("/api/deleteJobDetails", deleteJobDetails);
app.use("/api", getJobList);
app.use("/api", getSearchJobs);
app.use("/api", applyJobs);
// app.use("/api/myProfile", myProfile);

//max file size
// app.use(
//   fileUpload({
//     limits: { fileSize: 10 * 1024 * 1024 },
//   })
// );

//Routing Homepage
app.get("/", (req, res) => res.send("Welcome to HIRED API"));

//404 Not found
app.all("*", (req, res) =>
  res.send("You've tried reaching a route that doesn't exist.")
);

//Require DB Connection
const dbConnection = require("./databases/config/index")();

//Run Server
app.listen(PORT, () => {
  console.log(`Server is running on PORT ${PORT}.`);
});
