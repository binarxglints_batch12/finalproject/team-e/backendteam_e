//require model
const mongoose = require("mongoose");
const {
  salary_rangeModel,
} = require("../../databases/models/salaryRangeModel");

//Get all SalaryRange
const getAllSalaryRange = async (req, res) => {
  try {
    const data = await salary_rangeModel.find();

    res.send({
      statusCode: 200,
      statusText: "success",
      data,
    });
  } catch (error) {
    console.log(error);
    res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: error,
    });
  }
};

//Get a SalaryRange by ID
const getSalaryRange = async (req, res) => {
  const { id: _id } = req.params;

  if (!mongoose.Types.ObjectId.isValid(_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await salary_rangeModel.findById(_id);

    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      res.send({
        statusCode: 200,
        statusText: "success",
        data,
      });
    }
  }
};

//Create SalaryRange
const createSalaryRange = async (req, res) => {
  const newSalaryRange = await salary_rangeModel(req.body);
  try {
    await newSalaryRange.save();
    res.status(201).json(newSalaryRange);
  } catch (error) {
    console.log(error);
    res.status(409).json(error);
  }
};

//Update SalaryRange
const updateSalaryRange = async (req, res) => {
  const { id: _id } = req.params;
  const newData = req.body;

  if (!mongoose.Types.ObjectId.isValid(_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await salary_rangeModel.findById(_id);
    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      const updatedSalaryRange = await salary_rangeModel.findByIdAndUpdate(
        _id,
        newData,
        {
          new: true,
        }
      );
      return res.send({
        statusCode: 200,
        statusText: "success",
        data: updatedSalaryRange,
      });
    }
  }
};

//Delete SalaryRange by ID
const deleteSalaryRange = async (req, res) => {
  const id = req.params.id;

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await salary_rangeModel.findById(id);
    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      await salary_rangeModel.findByIdAndRemove(id);
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "SalaryRange deleted succesfully",
      });
    }
  }
};

module.exports = {
  getAllSalaryRange,
  getSalaryRange,
  createSalaryRange,
  updateSalaryRange,
  deleteSalaryRange,
};
