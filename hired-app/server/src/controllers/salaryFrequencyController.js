//require model
const mongoose = require("mongoose");
const {
  salary_frequencyModel,
} = require("../../databases/models/salaryFrequencyModel");

//Get all Salary_frequency
const getAllSalary_frequency = async (req, res) => {
  try {
    const data = await salary_frequencyModel.find();

    res.send({
      statusCode: 200,
      statusText: "success",
      data,
    });
  } catch (error) {
    console.log(error);
    res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: error,
    });
  }
};

//Get a Salary_frequency by ID
const getSalary_frequency = async (req, res) => {
  const { id: _id } = req.params;

  if (!mongoose.Types.ObjectId.isValid(_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await salary_frequencyModel.findById(_id);

    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      res.send({
        statusCode: 200,
        statusText: "success",
        data,
      });
    }
  }
};

//Create Salary_frequency
const createSalary_frequency = async (req, res) => {
  const newSalary_frequency = await salary_frequencyModel(req.body);
  try {
    await newSalary_frequency.save();
    res.status(201).json(newSalary_frequency);
  } catch (error) {
    console.log(error);
    res.status(409).json(error);
  }
};

//Update Salary_frequency
const updateSalary_frequency = async (req, res) => {
  const { id: _id } = req.params;
  const newData = req.body;

  if (!mongoose.Types.ObjectId.isValid(_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await salary_frequencyModel.findById(_id);
    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      const updatedSalary_frequency =
        await salary_frequencyModel.findByIdAndUpdate(_id, newData, {
          new: true,
        });
      return res.send({
        statusCode: 200,
        statusText: "success",
        data: updatedSalary_frequency,
      });
    }
  }
};

//Delete Salary_frequency by ID
const deleteSalary_frequency = async (req, res) => {
  const id = req.params.id;

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await salary_frequencyModel.findById(id);
    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      await salary_frequencyModel.findByIdAndRemove(id);
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Salary_frequency deleted succesfully",
      });
    }
  }
};

module.exports = {
  getAllSalary_frequency,
  getSalary_frequency,
  createSalary_frequency,
  updateSalary_frequency,
  deleteSalary_frequency,
};
