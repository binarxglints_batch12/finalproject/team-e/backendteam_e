const mongoose = require("mongoose");

const { applyJobModel } = require("../../databases/models/applyJobModels");

//POST "APPLY- JOBS"
const applyJobs = async (req, res) => {
  try {
    const { users_id, jobDetails_id } = req.body;
    const data = await applyJobModel.create({ users_id, jobDetails_id });
    // const data = await applyJobModel.create(
    //   {
    //     "Users.firstName": {
    //       $regex: users_id,
    //       $options: "i",
    //     },
    //   },
    //   {
    //     "jobs.job_title": {
    //       $regex: jobDetails_id,
    //       $options: "i",
    //     },
    //   }

    const newData = await applyJobModel
      .findById(applyJobs._id)
      .populate("users_id")
      .populate("jobDetails_id");
    console.log(newData);
    res.send({
      statusCode: 200,
      statusText: "Success",
      message: "Apply Job succeed!",
      result: [data, newData],
    });
  } catch (error) {
    console.log(error);
  }
};
//GET APPLIED JOBS BY USER ID
// DELETE APPLIED JOBS BY USER ID
module.exports = { applyJobs };
