const mongoose = require("mongoose")
const {UserModel} = require('../../databases/models//userModel')
const fs = require("fs")

const bcrpyt = require("bcrypt")


const createUser = async (req, res)  => {

    const existUser = await UserModel.findOne({ email: req.body.email });

  if (existUser) {
    res.status(409).json({
      statusCode: 409,
      statusText: "Fail",
      message: "Email Already Exist, please use another email address.",
    });
  }

    const newUser = new UserModel(req.body)
    
    try {
        const createdUser = await UserModel.create({
            // firstName = req.body.firstName,
            // lastName = req.body.lastName,
            // password = req.body.password,
            // email = req.body.email
            ...req.body
        })

        const newUser = await UserModel.findOne({ email: req.body.email })
        return res.status(201).json({
            statusCode: 200,
            message:" Registration Success",
            statuesText:"OK",
            data: {
                id: newUser._id,
                firstName: newUser.firstName,
                lastName: newUser.lastName,
                email: newUser.email
            }
        })
    } catch (error) {
        console.log(error);
        res.status(409).json({
          statusCode: 409,
          message: "Registration Failed.",
          statusText: "error",
        });
    }
}

const updateUser = async(req, res) => {
    const { id: _id } = req.params
    const newData = req.body

    if(!mongoose.Types.ObjectId.isValid(_id)) {
        return res.send({
            statusCode: 500,
            statusText: "fail",
            statusMessage: "format id invalid",
        })
    } else {
        const data = await UserModel.findById(_id)
        if (!data) {
            return res.send({
                statusCode: 500,
                statusText: "fail",
                statusMessage: "id not found",
            })
        } else {
            // if (!req.files) {
      //   return res.status(400).json({ message: "File is required" });
      // }

      // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
      // const profilePicture = req.files.profile_pic;
      // const uploadPath = __dirname + "/../public/images/" + profilePicture.name;

      // const moveImage = promisify(profilePicture.mv);

      // await moveImage(uploadPath);

      // const oldData = await UserModel.findOne({ _id: req.params.id });

      // Delete old file
      // await deleteFile(__dirname + "/../public/" + oldData.profile_pic);

            try {
                const update = await UserModel.findOneAndUpdate(
                    {_id: req.params.id},
                    {
                        ...req.body,
                         // profile_pic: `${profilePicture.name}`,
                    }
                )

                const data = await UserModel.findOne({ _id: req.params.id})

                return res.send({
                    statusCode: 200,
                    statusText: "succes",
                    data: data,
                })
            } catch (error) {
                res.status(409).json({
                    statusCode: 409,
                    message: "Update Failed.",
                    statusText: "error",
                  });
            }

        }
    }


}

const getAllUsers = async (req, res) => {
    try {
      data = await UserModel.find();
  
      res.json({
        statusCode: 200,
        statusText: "success",
        data,
      });
    } catch (error) {
      console.log(error);
      res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: error,
      });
    }
  };

module.exports = {
    createUser,
    updateUser,
    getAllUsers
}