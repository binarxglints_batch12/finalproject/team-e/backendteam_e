//require model
const mongoose = require("mongoose");
const {
  jobPostingCategoryModel,
} = require("../../databases/models/jobpostingCategory_model");

//Get all jobPostingCategory
const getAlljobPostingCategory = async (req, res) => {
  try {
    const data = await jobPostingCategoryModel.find();

    res.send({
      statusCode: 200,
      statusText: "success",
      data,
    });
  } catch (error) {
    console.log(error);
    res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: error,
    });
  }
};

//Get a jobPostingCategory by ID
const getjobPostingCategory = async (req, res) => {
  const { id: _id } = req.params;

  if (!mongoose.Types.ObjectId.isValid(_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await jobPostingCategoryModel.findById(_id);

    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      res.send({
        statusCode: 200,
        statusText: "success",
        data,
      });
    }
  }
};

//Create jobPostingCategory
const createjobPostingCategory = async (req, res) => {
  const newjobPostingCategory = await jobPostingCategoryModel(req.body);
  try {
    await newjobPostingCategory.save();
    res.status(201).json(newjobPostingCategory);
  } catch (error) {
    console.log(error);
    res.status(409).json(error);
  }
};

//Update jobPostingCategory
const updatejobPostingCategory = async (req, res) => {
  const { id: _id } = req.params;
  const newData = req.body;

  if (!mongoose.Types.ObjectId.isValid(_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await jobPostingCategoryModel.findById(_id);
    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      const updatedjobPostingCategory =
        await jobPostingCategoryModel.findByIdAndUpdate(_id, newData, {
          new: true,
        });
      return res.send({
        statusCode: 200,
        statusText: "success",
        data: updatedjobPostingCategory,
      });
    }
  }
};

//Delete jobPostingCategory by ID
const deletejobPostingCategory = async (req, res) => {
  const id = req.params.id;

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await jobPostingCategoryModel.findById(id);
    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      await jobPostingCategoryModel.findByIdAndRemove(id);
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "jobPostingCategory deleted succesfully",
      });
    }
  }
};

module.exports = {
  getAlljobPostingCategory,
  getjobPostingCategory,
  createjobPostingCategory,
  updatejobPostingCategory,
  deletejobPostingCategory,
};
