//require model
const mongoose = require("mongoose");
const { companyModel } = require("../../databases/models/companyModel");
const Moment = require("moment");

//Get all companys
const getAllCompany = async (req, res) => {
  try {
    const data = await companyModel.find();

    res.send({
      statusCode: 200,
      statusText: "success",
      data,
    });
  } catch (error) {
    console.log(error);
    res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: error,
    });
  }
};

//search by List Company
const getSearchCompany = async (req, res) => {
  const { industry_categories, location, company_name, company_size } =
    req.query;
  let queryDb = [];
  if (undefined !== industry_categories) {
    queryDb.push({
      "industry_categories.description": {
        $regex: industry_categories.replace(",", "|"),
        $options: "i",
      },
    });
  }
  if (undefined !== location) {
    queryDb.push({ location: { $regex: location, $options: "i" } });
  }

  if (undefined !== company_name) {
    queryDb.push({
      company_name: { $regex: company_name, $options: "i" },
    });
  }

  if (undefined !== company_size) {
    queryDb.push({
      "company_size.description": {
        $regex: company_size.replace(",", "|"),
        $options: "i",
      },
    });
  }

  console.log(
    "🚀 ~ file: companyController.js ~ line 61 ~ getSearchCompany ~ queryDb",
    queryDb
  );
  const data = await companyModel.find({
    $and: queryDb,
  });

  console.log(
    "🚀 ~ file: companyController.js ~ line 37 ~ getSearchCompany ~ data",
    data
  );

  if (data.length === 0) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Company Name Not Found",
    });
  } else {
    res.send({
      statusCode: 200,
      statusText: "success",
      data,
    });
  }
};

//! jangan Dihapus
// console.log(
//   "🚀 ~ file: companyController.js ~ line 37 ~ getSearchCompany ~ data",
//   data
// );

// if (data.length === 0) {
//   return res.send({
//     statusCode: 500,
//     statusText: "fail",
//     statusMessage: "Company Name Not Found",
//   });
// } else {
//   res.send({
//     statusCode: 200,
//     statusText: "success",
//     data,
//   });
// }

//Get a company by Company Name
// const getSearchCompany = async (req, res) => {
//   try {
//     const dataParams = req.params;
//     console.log("🚀 ~ file: companyController.js ~ line 79 ~ getSearchCompany ~ dataParams", dataParams)

//     console.log(req.params.location)

//     const data = await companyModel.find({

//       $and: [
//         { $or:[

// {"company_name": { $regex: '${req.params.company_name}', $options: 'i'}},
//           {"location": { $regex: '${req.params.location}', $options: 'i'}},
//           {"industry_categories": { $regex: '${req.params.industry_categories}', $options: 'i'}},
//           {"company_size": { $regex: '${req.params.company_size}', $options: 'i'}}
//         ]
//       },
//       ]
//     });

//       if (data.length=== 0 ) {
//         return res.send({
//           statusCode: 500,
//           statusText: "fail",
//           statusMessage: "Company Name Not Found",
//         });
//       } else {
//         res.send({
//           statusCode: 200,
//           statusText: "success",
//           data,
//         });
//       }
//     } catch (error) {
//       res
//         .json({
//           statusCode: 500,
//           statusText: "error",
//           message: error.message,
//         })
//         .status(500);
//     }
// }

// ! Jangan dihapus
//Get a company by ID
// const getByIdCompany = async (req, res) => {
//   const { id: _id } = req.params;

//   if (!mongoose.Types.ObjectId.isValid(_id)) {
//     return res.send({
//       statusCode: 500,
//       statusText: "fail",
//       statusMessage: "Format Id invalid lalalal",
//     });
//   } else {
//     const data = await companyModel.findById(_id);

//     if (!data) {
//       return res.send({
//         statusCode: 500,
//         statusText: "fail",
//         statusMessage: "Id not found",
//       });
//     } else {
//       res.send({
//         statusCode: 200,
//         statusText: "success",
//         data,
//       });
//     }
//   }
// };

//Create company
const createCompany = async (req, res) => {
  const newCompany = await companyModel(req.body);
  console.log(
    "🚀 ~ file: companyController.js ~ line 227 ~ createCompany ~ newCompany",
    newCompany
  );
  try {
    await newCompany.save();
    res.status(201).json(newCompany);
  } catch (error) {
    console.log(error);
    res.status(409).json(error);
  }
};

//Update company
const updateCompany = async (req, res) => {
  const { id: _id } = req.params;
  const newData = req.params;

  if (!mongoose.Types.ObjectId.isValid(_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await companyModel.findById(_id);
    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      const updatedcompany = await companyModel.findByIdAndUpdate(
        _id,
        newData,
        {
          new: true,
        }
      );
      return res.send({
        statusCode: 200,
        statusText: "success",
        data: updatedcompany,
      });
    }
  }
};

//Delete company by ID
const deleteCompany = async (req, res) => {
  const id = req.params.id;

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await companyModel.findById(id);
    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      await companyModel.findByIdAndRemove(id);
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "company deleted succesfully",
      });
    }
  }
};

module.exports = {
  getAllCompany,
  getSearchCompany,
  // getByIdCompany,
  createCompany,
  updateCompany,
  deleteCompany,
};
