//require model
const mongoose = require("mongoose");
const { jobTypesModel } = require("../../databases/models/jobTypesModel");

//Get all jobTypes
const getAlljobTypes = async (req, res) => {
  try {
    const data = await jobTypesModel.find();

    res.send({
      statusCode: 200,
      statusText: "success",
      data,
    });
  } catch (error) {
    console.log(error);
    res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: error,
    });
  }
};

//Get a jobType by ID
const getjobTypes = async (req, res) => {
  const { id: _id } = req.params;

  if (!mongoose.Types.ObjectId.isValid(_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await jobTypesModel.findById(_id);

    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      res.send({
        statusCode: 200,
        statusText: "success",
        data,
      });
    }
  }
};

//Create jobType
const createjobTypes = async (req, res) => {
  const newjobType = await jobTypesModel(req.body);
  try {
    await newjobType.save();
    res.status(201).json(newjobType);
  } catch (error) {
    console.log(error);
    res.status(409).json(error);
  }
};

//Update jobType
const updatejobTypes = async (req, res) => {
  const { id: _id } = req.params;
  const newData = req.body;

  if (!mongoose.Types.ObjectId.isValid(_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await jobTypesModel.findById(_id);
    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      const updatedjobType = await jobTypesModel.findByIdAndUpdate(
        _id,
        newData,
        {
          new: true,
        }
      );
      return res.send({
        statusCode: 200,
        statusText: "success",
        data: updatedjobType,
      });
    }
  }
};

//Delete jobType by ID
const deletejobTypes = async (req, res) => {
  const id = req.params.id;

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await jobTypesModel.findById(id);
    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      await jobTypesModel.findByIdAndRemove(id);
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "jobType deleted succesfully",
      });
    }
  }
};

module.exports = {
  getAlljobTypes,
  getjobTypes,
  createjobTypes,
  updatejobTypes,
  deletejobTypes,
};
