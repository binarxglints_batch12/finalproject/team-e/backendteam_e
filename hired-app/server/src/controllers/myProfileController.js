const mongoose = require("mongoose");
const { userModel } = require("../../databases/models/userModel");

//get user profile
const getUserProfile = async (req, res) => {
    const { id: _id } = req.params;
  
    if (!mongoose.Types.ObjectId.isValid(_id)) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Success",
      });
    } else {
      const data = await userModel.findById(_id);
  
      if (!data) {
        return res.send({
          statusCode: 500,
          statusText: "fail",
          statusMessage: "Id not found",
        });
      } else {
        res.send({
          statusCode: 200,
          statusText: "success",
          data,
        });
      }
    }
  };

//update user profile
  const updateUserProfile = async (req, res) => {
    const { id: _id } = req.params;
    const newData = req.body;
  
    if (!mongoose.Types.ObjectId.isValid(_id)) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "fail to update",
      });
    } else {
      const data = await userModel.findById(_id);
      if (!data) {
        return res.send({
          statusCode: 500,
          statusText: "fail",
          statusMessage: "Id not found",
        });
      } else {
        const updateUserProfile = await userModel.findByIdAndUpdate(
          _id,
          newData,
          {
            new: true,
          }
        );
        return res.send({
          statusCode: 200,
          statusText: "success",
          data: updateUserProfile,
        });
      }
    }
  };

  //post user profile
const postUserProfile = async (req, res) => {
    const newUserProfile = await userModel(req.body);
    try {
      await newUserProfile.save();
      res.status(201).json(newUserProfile);
    } catch (error) {
      console.log(error);
      res.status(409).json(error);
    }
  };

  //get salary expectation
  const getSalaryExpectation = async (req, res) => {
      const { id: _id } = req.params;
    
      if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.send({
          statusCode: 500,
          statusText: "fail",
          statusMessage: "Success",
        });
      } else {
        const data = await userModel.findById(_id);
    
        if (!data) {
          return res.send({
            statusCode: 500,
            statusText: "fail",
            statusMessage: "Id not found",
          });
        } else {
          res.send({
            statusCode: 200,
            statusText: "success",
            data,
          });
        }
      }
    };
  
  //update salary expectation
    const updateSalaryExpectation = async (req, res) => {
      const { id: _id } = req.params;
      const newData = req.body;
    
      if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.send({
          statusCode: 500,
          statusText: "fail",
          statusMessage: "fail to update",
        });
      } else {
        const data = await userModel.findById(_id);
        if (!data) {
          return res.send({
            statusCode: 500,
            statusText: "fail",
            statusMessage: "Id not found",
          });
        } else {
          const updateSalaryExpectation = await userModel.findByIdAndUpdate(
            _id,
            newData,
            {
              new: true,
            }
          );
          return res.send({
            statusCode: 200,
            statusText: "success",
            data: updateSalaryExpectation,
          });
        }
      }
    };
  
    //post salary expectation
  const postSalaryExpectation = async (req, res) => {
      const newSalaryExpecation = await userModel(req.body);
      try {
        await newSalaryExpecation.save();
        res.status(201).json(newSalaryExpecation);
      } catch (error) {
        console.log(error);
        res.status(409).json(error);
      }
    };

//get work experience
const getWorkExperience = async (req, res) => {
    const { id: _id } = req.params;
  
    if (!mongoose.Types.ObjectId.isValid(_id)) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Success",
      });
    } else {
      const data = await userModel.findById(_id);
  
      if (!data) {
        return res.send({
          statusCode: 500,
          statusText: "fail",
          statusMessage: "Id not found",
        });
      } else {
        res.send({
          statusCode: 200,
          statusText: "success",
          data,
        });
      }
    }
  };

//update work experience
  const updateWorkExperience = async (req, res) => {
    const { id: _id } = req.params;
    const newData = req.body;
  
    if (!mongoose.Types.ObjectId.isValid(_id)) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "fail to update",
      });
    } else {
      const data = await userModel.findById(_id);
      if (!data) {
        return res.send({
          statusCode: 500,
          statusText: "fail",
          statusMessage: "Id not found",
        });
      } else {
        const updateWorkExperience = await userModel.findByIdAndUpdate(
          _id,
          newData,
          {
            new: true,
          }
        );
        return res.send({
          statusCode: 200,
          statusText: "success",
          data: updateSalaryExpectation,
        });
      }
    }
  };

  //post work experience
const postWorkExperience = async (req, res) => {
    const newWorkExperience = await userModel(req.body);
    try {
      await newWorkExperience.save();
      res.status(201).json(newWorkExperience);
    } catch (error) {
      console.log(error);
      res.status(409).json(error);
    }
  };

//get education
const getEducation = async (req, res) => {
    const { id: _id } = req.params;
  
    if (!mongoose.Types.ObjectId.isValid(_id)) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Success",
      });
    } else {
      const data = await userModel.findById(_id);
  
      if (!data) {
        return res.send({
          statusCode: 500,
          statusText: "fail",
          statusMessage: "Id not found",
        });
      } else {
        res.send({
          statusCode: 200,
          statusText: "success",
          data,
        });
      }
    }
  };

//update education
  const updateEducation = async (req, res) => {
    const { id: _id } = req.params;
    const newData = req.body;
  
    if (!mongoose.Types.ObjectId.isValid(_id)) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "fail to update",
      });
    } else {
      const data = await userModel.findById(_id);
      if (!data) {
        return res.send({
          statusCode: 500,
          statusText: "fail",
          statusMessage: "Id not found",
        });
      } else {
        const updateEducation = await userModel.findByIdAndUpdate(
          _id,
          newData,
          {
            new: true,
          }
        );
        return res.send({
          statusCode: 200,
          statusText: "success",
          data: updateEducation,
        });
      }
    }
  };

  //post education
const postEducation = async (req, res) => {
    const newEducation = await userModel(req.body);
    try {
      await newUserProfile.save();
      res.status(201).json(newUserProfile);
    } catch (error) {
      console.log(error);
      res.status(409).json(error);
    }
  };

//get skill
const getSkill = async (req, res) => {
    const { id: _id } = req.params;
  
    if (!mongoose.Types.ObjectId.isValid(_id)) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Success",
      });
    } else {
      const data = await userModel.findById(_id);
  
      if (!data) {
        return res.send({
          statusCode: 500,
          statusText: "fail",
          statusMessage: "Id not found",
        });
      } else {
        res.send({
          statusCode: 200,
          statusText: "success",
          data,
        });
      }
    }
  };

//update skill
  const updateSkill = async (req, res) => {
    const { id: _id } = req.params;
    const newData = req.body;
  
    if (!mongoose.Types.ObjectId.isValid(_id)) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "fail to update",
      });
    } else {
      const data = await userModel.findById(_id);
      if (!data) {
        return res.send({
          statusCode: 500,
          statusText: "fail",
          statusMessage: "Id not found",
        });
      } else {
        const updateSkill = await userModel.findByIdAndUpdate(
          _id,
          newData,
          {
            new: true,
          }
        );
        return res.send({
          statusCode: 200,
          statusText: "success",
          data: updateSkill,
        });
      }
    }
  };

  //post skill
const postSkill = async (req, res) => {
    const newSkill = await userModel(req.body);
    try {
      await newSkill.save();
      res.status(201).json(newSkill);
    } catch (error) {
      console.log(error);
      res.status(409).json(error);
    }
  };

  //get resume
const getResume = async (req, res) => {
    const { id: _id } = req.params;
  
    if (!mongoose.Types.ObjectId.isValid(_id)) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Success",
      });
    } else {
      const data = await userModel.findById(_id);
  
      if (!data) {
        return res.send({
          statusCode: 500,
          statusText: "fail",
          statusMessage: "Id not found",
        });
      } else {
        res.send({
          statusCode: 200,
          statusText: "success",
          data,
        });
      }
    }
  };

//update resume
  const updateResume = async (req, res) => {
    const { id: _id } = req.params;
    const newData = req.body;
  
    if (!mongoose.Types.ObjectId.isValid(_id)) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "fail to update",
      });
    } else {
      const data = await userModel.findById(_id);
      if (!data) {
        return res.send({
          statusCode: 500,
          statusText: "fail",
          statusMessage: "Id not found",
        });
      } else {
        const updateResume = await userModel.findByIdAndUpdate(
          _id,
          newData,
          {
            new: true,
          }
        );
        return res.send({
          statusCode: 200,
          statusText: "success",
          data: updateResume,
        });
      }
    }
  };

  //post resume
const postResume = async (req, res) => {
    const newUserProfile = await userModel(req.body);
    try {
      await newResume.save();
      res.status(201).json(newResume);
    } catch (error) {
      console.log(error);
      res.status(409).json(error);
    }
  };


  module.exports = {
    getUserProfile,
    postUserProfile,
    updateUserProfile,
    getSalaryExpectation,
    postSalaryExpectation,
    updateSalaryExpectation,
    getWorkExperience,
    postWorkExperience,
    updateWorkExperience,
    getEducation,
    postEducation,
    updateEducation,
    getSkill,
    postSkill,
    updateSkill,
    getResume,
    postResume,
    updateResume,
  };
  

  




  
  
  
