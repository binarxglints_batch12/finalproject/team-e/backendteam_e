const mongoose = require("mongoose");

// Require Model

const { jobsModel } = require("../../databases/models/jobsModel");
const { companyModel } = require("../../databases/models/companyModel");
const e = require("cors");

//create Job detail
const postJobDetail = async (req, res) => {
  try {
    const job = new jobsModel();
    job.job_title = req.body.title;
    job.description = req.body.description;
    job_responsibilities = req.body.job_responsibilities;
    job_requirements = req.body.job_requirements;
    job.date_Posted = req.body.date_Posted;
    job.expired_date = req.body.expired_date;
    job.work_hour = req.body.work_hour;
    job.salary = req.body.salary;
    job.isBookmark = req.body.isBookmark;
    job.company_id = req.body.company_id;

    //  embedded
    job.Job_posting_category.push(req.body.Job_posting_category);
    job.job_types.push(req.body.job_types);
    job.experience_levels.push(req.body.experience_levels);
    job.salary_frequency.push(req.body.salary_frequency);
    job.salary_range.push(req.body.salary_range);

    //  another way
    //  model.create({

    //  }, {
    //    $push: {

    //    }
    //  })

    job.save((err, job, next) => {
      if (err) {
        res.json({
          statusCode: 200,
          statusText: "ok",
          message: "Failed to add new Job details",
          error: err,
        });
      } else {
        res.send({
          statusCode: 200,
          statusText: "ok",
          message: "Succeed to add new Job details",
        });
      }
    });
  } catch (error) {
    console.log("ERROR: ", error);
    return res.send({
      statusCode: 500,
      statusText: "Failed to add new Job details!",
      error,
    });
  }
};

// Get Search Jobs
const getSearchJobs = async (req, res) => {
  const {
    industry_categories,
    job_types,
    experience_levels,
    salary_frequency,
    salary_range,
    Job_posting_category,
    job_title,
    location,
  } = req.query;
  let queryDb = [];

  if (undefined !== job_types) {
    queryDb.push({
      "job_types.description": {
        $regex: job_types.replace(",", "|"),
        $options: "i",
      },
    });
  }

  if (undefined !== experience_levels) {
    queryDb.push({
      "experience_levels.description": {
        $regex: experience_levels.replace(",", "|"),
        $options: "i",
      },
    });
  }

  if (undefined !== salary_frequency) {
    queryDb.push({
      "salary_frequency.description": {
        $regex: salary_frequency.replace(",", "|"),
        $options: "i",
      },
    });
  }

  if (undefined !== salary_range) {
    queryDb.push({
      "salary_range.description": {
        $regex: salary_range.replace(",", "|"),
        $options: "i",
      },
    });
  }

  if (undefined !== Job_posting_category) {
    queryDb.push({
      "Job_posting_category.description": {
        $regex: Job_posting_category.replace(",", "|"),
        $options: "i",
      },
    });
  }

  if (undefined !== job_title) {
    queryDb.push({
      job_title: {
        $regex: job_title.replace(",", "|"),
        $options: "i",
      },
    });
  }

  console.log(queryDb);
  const data = await jobsModel
    .find({ $and: queryDb })
    .select({
      _id: 0,
      job_title: 1,
      salary: 1,
      job_types: 1,
    })
    .populate({
      path: "company_id",
      select: [
        "company_name",
        "company_logo",
        "location",
        "industry_categories",
      ],
    })

    .exec((err, result) => {
      if (err) {
        res.send({
          statusCode: 404,
          statusText: "Jobs Not Found",
          message: err,
        });
      } else {
        let data = result.filter(function (el) {
          if (industry_categories) {
            return (
              el.company_id.industry_categories.filter((e) =>
                industry_categories
                  ? industry_categories.split(",").includes(e.description)
                  : true
              ).length > 0
            );
          }
          if (location) {
            return (
              el.company_id.location.filter((e) =>
                location ? location.split(",") : true
              ).length > 0
            );
          }
        });
        res.send({
          statusCode: 200,
          statusText: "ok",
          list_jobs: data.length,
          data: data,
        });
      }
    });
  console.log(data);
};

// Get Joblist
const getJobList = async (req, res) => {
  try {
    const jobList = await jobsModel
      .find({})
      .select({
        _id: 0,
        job_title: 1,
        salary: 1,
        job_types: 1,
      })
      .populate({
        path: "company_id",
        select: ["company_name", "company_logo", "location"],
      })
      .exec((err, result) => {
        if (err) {
          res.send({
            statusCode: 404,
            statusText: "Not Found",
            message: err,
          });
        } else {
          res.send({
            statusCode: 200,
            statusText: "ok",
            data: result,
            jumlah: result.length,
          });
        }
      });
  } catch (error) {
    console.log(error);
  }
};

//Get ALL Job Details
const getJobDetails = async (req, res) => {
  try {
    const jobList = jobsModel
      .find({})
      .select({
        job_title: 1,
        salary: 1,
        job_types: 1,
        description: 1,
        job_responsibilities: 1,
        Job_posting_category: 1,
        job_requirements: 1,
        date_Posted: 1,
        expired_date: 1,
        experience_levels: 1,
        work_hour: 1,
        salary_range: 1,
      })
      .populate({
        path: "company_id",
        select: ["company_name", "company_logo", "location"],
      });
    jobList.exec(async (err, result) => {
      const relatedJobs = await jobsModel
        .find({ job_title: { $regex: result[0].job_title, $options: "i" } })
        .select({
          job_title: 1,
          job_types: 1,
          salary: 1,
        })
        .populate({
          path: "company_id",
          select: ["company_name", "company_logo", "location"],
        });

      const filtered = relatedJobs.filter((item) => item._id === result[0]._id);
      if (err) {
        console.log(err);
        res.send({
          statusCode: 404,
          statusText: "Not Found",
          message: err,
        });
      } else {
        res.send({
          statusCode: 200,
          statusText: "ok",
          data: [...result, { related_jobs: filtered }],
        });
      }
    });
  } catch (error) {
    console.log(error);
    res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: error,
    });
  }
};

//Get Job Details by ID
//update Jobdetails
const updateJobDetails = async (req, res) => {
  const { id: _id } = req.params;
  const newData = req.body;

  if (!mongoose.Types.ObjectId.isValid(_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await jobsModel.findById(_id);
    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      const updatedJobDetails = await jobsModel.findByIdAndUpdate(
        _id,
        newData,
        {
          new: true,
        }
      );
      return res.send({
        statusCode: 200,
        statusText: "success",
        data: updatedJobDetails,
      });
    }
  }
};

//Delete Job Details by ID
const deleteJobDetails = async (req, res) => {
  const id = req.params.id;

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await jobsModel.findById(id);
    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      await jobsModel.findByIdAndRemove(id);
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Joblist deleted succesfully",
      });
    }
  }
};

module.exports = {
  postJobDetail,
  getJobDetails,
  updateJobDetails,
  deleteJobDetails,
  getJobList,
  getSearchJobs,
};
