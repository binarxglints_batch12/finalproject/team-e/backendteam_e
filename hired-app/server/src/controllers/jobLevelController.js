//require model
const mongoose = require("mongoose");
const { jobLevelModel } = require("../../databases/models/jobLevelModel");

//Get all jobLevels
const getAlljobLevels = async (req, res) => {
  try {
    const data = await jobLevelModel.find();

    res.send({
      statusCode: 200,
      statusText: "success",
      data,
    });
  } catch (error) {
    console.log(error);
    res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: error,
    });
  }
};

//Get a jobLevel by ID
const getjobLevel = async (req, res) => {
  const { id: _id } = req.params;

  if (!mongoose.Types.ObjectId.isValid(_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await jobLevelModel.findById(_id);

    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      res.send({
        statusCode: 200,
        statusText: "success",
        data,
      });
    }
  }
};

//Create jobLevel
const createjobLevel = async (req, res) => {
  const newjobLevel = await jobLevelModel(req.body);
  try {
    await newjobLevel.save();
    res.status(201).json(newjobLevel);
  } catch (error) {
    console.log(error);
    res.status(409).json(error);
  }
};

//Update jobLevel
const updatejobLevel = async (req, res) => {
  const { id: _id } = req.params;
  const newData = req.body;

  if (!mongoose.Types.ObjectId.isValid(_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await jobLevelModel.findById(_id);
    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      const updatedjobLevel = await jobLevelModel.findByIdAndUpdate(
        _id,
        newData,
        {
          new: true,
        }
      );
      return res.send({
        statusCode: 200,
        statusText: "success",
        data: updatedjobLevel,
      });
    }
  }
};

//Delete jobLevel by ID
const deletejobLevel = async (req, res) => {
  const id = req.params.id;

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await jobLevelModel.findById(id);
    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      await jobLevelModel.findByIdAndRemove(id);
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "jobLevel deleted succesfully",
      });
    }
  }
};

module.exports = {
  getAlljobLevels,
  getjobLevel,
  createjobLevel,
  updatejobLevel,
  deletejobLevel,
};
