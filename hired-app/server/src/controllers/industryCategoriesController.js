//require model
const mongoose = require("mongoose");
const {
  industry_categoriesModel,
} = require("../../databases/models/industry_categories");

//Get all industry_categories
const getAllindustry_categories = async (req, res) => {
  try {
    const data = await industry_categoriesModel.find();

    res.send({
      statusCode: 200,
      statusText: "success",
      data,
    });
  } catch (error) {
    console.log(error);
    res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: error,
    });
  }
};

//Get a industry_categories by industry_categories Name
const getByNameindustry_categories = async (req, res) => {
  const { industry_categoriesName: industry_categories_name } = req.params;
  const data = await industry_categoriesModel.find({
    industry_categories_name: { $regex: req.params.industry_categories_name },
  });
  if (data.length === 0) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "industry_categories Name Not Found",
    });
  } else {
    res.send({
      statusCode: 200,
      statusText: "success",
      data,
    });
  }
};

//Get a industry_categories by ID
const getByIdindustry_categories = async (req, res) => {
  const { id: _id } = req.params;

  if (!mongoose.Types.ObjectId.isValid(_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await industry_categoriesModel.findById(_id);

    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      res.send({
        statusCode: 200,
        statusText: "success",
        data,
      });
    }
  }
};

//Create industry_categories
const createindustry_categories = async (req, res) => {
  const newindustry_categories = await industry_categoriesModel(req.body);
  try {
    await newindustry_categories.save();
    res.status(201).json(newindustry_categories);
  } catch (error) {
    console.log(error);
    res.status(409).json(error);
  }
};

//Update industry_categories
const updateindustry_categories = async (req, res) => {
  const { id: _id } = req.params;
  const newData = req.body;

  if (!mongoose.Types.ObjectId.isValid(_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await industry_categoriesModel.findById(_id);
    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      const updatedindustry_categories =
        await industry_categoriesModel.findByIdAndUpdate(_id, newData, {
          new: true,
        });
      return res.send({
        statusCode: 200,
        statusText: "success",
        data: updatedindustry_categories,
      });
    }
  }
};

//Delete industry_categories by ID
const deleteindustry_categories = async (req, res) => {
  const id = req.params.id;

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await industry_categoriesModel.findById(id);
    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      await industry_categoriesModel.findByIdAndRemove(id);
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "industry_categories deleted succesfully",
      });
    }
  }
};

module.exports = {
  getAllindustry_categories,
  getByNameindustry_categories,
  getByIdindustry_categories,
  createindustry_categories,
  updateindustry_categories,
  deleteindustry_categories,
};
