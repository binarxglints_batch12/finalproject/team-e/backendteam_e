const bcrpyt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { UserModel } = require("../../databases/models/userModel");

const loginUser = async (req, res) => {
  const { email, password } = req.body;

  if (email && password) {
    const candidate = await UserModel.findOne({ email: email }).exec();

    if (candidate) {
      const { _id, email, firstName, lastName, role } = candidate;

      const verify = await bcrpyt.compare(
        req.body.password,
        candidate.password
      );

      if (verify === true) {
        jwt.sign(
          {
            candidate: {
              id: _id,
              firstName: firstName,
              lastName: lastName,
              email: email,
              role: role,
            },
          },
          process.env.TOKEN_SECRET,
          { expiresIn: "30d" },
          (error, token) => {
            if (error) throw error;

            res.header("Authorization", token).json({
              statusCode: 200,
              message: "Login Sucessfully",
              statustext: "OK",
              data: {
                id: _id,
                firstName: firstName,
                lastName: lastName,
                email: email,
                role: role,
                token: token,
              },
            });
          }
        );
      } else {
        res.send({
          statusCode: 204,
          message: "Wrong password!",
          statusText: "OK",
        });
      }
    } else {
      res.send({
        statusCode: 204,
        message: "Email has not registered, please create user account first",
        statusText: "OK",
      });
    }
  } else {
    res.send({
      statusCode: 403,
      message: "Please enter Email and Password",
      statusText: "OK",
    });
  }
};

const loginEmployee = async (req, res) => {
  const { email, password } = req.body;

  if (email && password) {
    const Employee = await UserModel.findOne({ email: email }).exec();

    if (Employee) {
      const { _id, email, firstName, lastName, role } = Employee;

      const verify = await bcrpyt.compare(req.body.password, Employee.password);

      if (verify === true) {
        jwt.sign(
          {
            Employee: {
              id: _id,
              firstName: firstName,
              lastName: lastName,
              email: email,
              role: role,
            },
          },
          process.env.TOKEN_SECRET,
          { expiresIn: "30d" },
          (error, token) => {
            if (error) throw error;

            res.header("Authorization", token).json({
              statusCode: 200,
              message: "Login Sucessfully",
              statustext: "OK",
              data: {
                id: _id,
                firstName: firstName,
                lastName: lastName,
                email: email,
                role: role,
                token: token,
              },
            });
          }
        );
      } else {
        res.send({
          statusCode: 204,
          message: "Wrong password!",
          statusText: "OK",
        });
      }
    } else {
      res.send({
        statusCode: 205,
        message:
          "Email has not registered, please create employee account first",
        statusText: "OK",
      });
    }
  } else {
    res.send({
      statusCode: 403,
      message: "Please enter Email and Password",
      statusText: "OK",
    });
  }
};

module.exports = {
  loginUser,
  loginEmployee,
};
