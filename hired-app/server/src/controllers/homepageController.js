const mongoose = require("mongoose");
const { companyModel } = require("../../databases/models/companyModel");
const { jobsModel } = require("../../databases/models/jobsModel");

const getSearchByAll = async (req, res) => {
  try {
    const companyDetails = await companyModel.find();
    const jobsDetails = await jobsModel.find();

    res.send({
      statusCode: 200,
      statusText: "success",
      data: {
        companyDetails,
        jobsDetails,
      },
    });
  } catch (error) {
    console.log(error);
    res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: error,
    });
  }
};

//Get Search Job by Title
const getSearchJobByTitle = async (req, res) => {
  const { job_title } = req.query;
  const industryCategorieDetails = await jobsModel
    .find({
      job_title: { $regex: req.query.job_title, $options: "i" },
    })
    .populate({ path: "company_id", select: "-createdAt -__v" });

  if (industryCategorieDetails.length === 0) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Job Name Not Found",
    });
  } else {
    res.send({
      statusCode: 200,
      statusText: "success",
      industryCategorieDetails,
    });
  }
};

//Get Popular Job Categories
const getPopularJobCategories = async (req, res) => {
  const { job_types } = req.query;
  console.log(
    "🚀 ~ file: homepageController.js ~ line 54 ~ getPopularJobCategories ~ job_types",
    job_types
  );
  const jobTypesDetails = await jobsModel
    .find({
      "job_types.description": { $regex: req.query.job_types, $options: "i" },
    })
    .populate({ path: "company_id", select: "-createdAt -__v" });
  console.log(
    "🚀 ~ file: homepageController.js ~ line 69 ~ getPopularJobCategories ~ jobTypesDetails",
    jobTypesDetails
  );
  if (jobTypesDetails.length === 0) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Job Based On Categories Not Found",
    });
  } else {
    res.send({
      statusCode: 200,
      statusText: "success",
      jobTypesDetails,
    });
  }
};

//Get Featured Job By Posting Category
const getFeatureJobByPost = async (req, res) => {
  const { Job_posting_category } = req.query;
  const jobPostDetails = await jobsModel
    .find({
      "Job_posting_category.description": {
        $regex: req.query.Job_posting_category,
        $options: "i",
      },
    })
    .populate({ path: "Job_posting_category" })
    .populate({ path: "company_id", select: "-createdAt -__v" });

  console.log(
    "🚀 ~ file: homepageController.js ~ line 77 ~ getFeatureJobByPost ~ jobPostDetails",
    jobPostDetails
  );
  if (jobPostDetails.length === 0) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Job Based On Post Category Not Found",
    });
  } else {
    res.send({
      statusCode: 200,
      statusText: "success",
      jobPostDetails,
    });
  }
};

//Get Top 5 Companies
const getTopCompanies = async (req, res) => {
  const { company_size } = req.query;
  const companySizeDetails = await companyModel
    .find({
      "company_size.description": {
        $regex: req.query.company_size,
        $options: "i",
      },
    })
    .populate("company_size", "company");
  console.log(
    "🚀 ~ file: homepageController.js ~ line 96 ~ getTopCompanies ~ companySizeDetails",
    companySizeDetails
  );
  if (companySizeDetails.length === 0) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Companies Based On Size Category Not Found",
    });
  } else {
    res.send({
      statusCode: 200,
      statusText: "success",
      companySizeDetails,
    });
  }
};

module.exports = {
  getSearchByAll,
  getSearchJobByTitle,
  getPopularJobCategories,
  getFeatureJobByPost,
  getTopCompanies,
};
