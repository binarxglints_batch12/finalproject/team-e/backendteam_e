//require model
const mongoose = require("mongoose");
const { CompanySizeModel } = require("../../databases/models/companySize");

//Get all CompanySizeSize
const getAllCompanySize = async (req, res) => {
  try {
    const data = await CompanySizeModel.find();

    res.send({
      statusCode: 200,
      statusText: "success",
      data,
    });
  } catch (error) {
    console.log(error);
    res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: error,
    });
  }
};

//Get a CompanySize by CompanySize Name
const getByNameCompanySize = async (req, res) => {
  const { CompanySizeName: CompanySize_name } = req.params;
  const data = await CompanySizeModel.find({
    CompanySize_name: { $regex: req.params.CompanySize_name },
  });
  if (data.length === 0) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "CompanySize Name Not Found",
    });
  } else {
    res.send({
      statusCode: 200,
      statusText: "success",
      data,
    });
  }
};

//Get a CompanySize by ID
const getByIdCompanySize = async (req, res) => {
  const { id: _id } = req.params;

  if (!mongoose.Types.ObjectId.isValid(_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await CompanySizeModel.findById(_id);

    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      res.send({
        statusCode: 200,
        statusText: "success",
        data,
      });
    }
  }
};

//Create CompanySize
const createCompanySize = async (req, res) => {
  const newCompanySize = await CompanySizeModel(req.body);
  try {
    await newCompanySize.save();
    res.status(201).json(newCompanySize);
  } catch (error) {
    console.log(error);
    res.status(409).json(error);
  }
};

//Update CompanySize
const updateCompanySize = async (req, res) => {
  const { id: _id } = req.params;
  const newData = req.body;

  if (!mongoose.Types.ObjectId.isValid(_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await CompanySizeModel.findById(_id);
    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      const updatedCompanySize = await CompanySizeModel.findByIdAndUpdate(
        _id,
        newData,
        {
          new: true,
        }
      );
      return res.send({
        statusCode: 200,
        statusText: "success",
        data: updatedCompanySize,
      });
    }
  }
};

//Delete CompanySize by ID
const deleteCompanySize = async (req, res) => {
  const id = req.params.id;

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await CompanySizeModel.findById(id);
    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      await CompanySizeModel.findByIdAndRemove(id);
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "CompanySize deleted succesfully",
      });
    }
  }
};

module.exports = {
  getAllCompanySize,
  getByNameCompanySize,
  getByIdCompanySize,
  createCompanySize,
  updateCompanySize,
  deleteCompanySize,
};
