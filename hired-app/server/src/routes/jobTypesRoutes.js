const express = require("express");

const route = express.Router();

const {
  getAlljobTypes,
  getjobTypes,
  createjobTypes,
  updatejobTypes,
  deletejobTypes,
} = require("../controllers/jobTypesController");

//Get All jobTypes
route.get("/", getAlljobTypes);

//Get single jobTypes by id
route.get("/:id", getjobTypes);

//Create jobTypes
route.post("/", createjobTypes);

//Update jobTypes
route.put("/:id", updatejobTypes);

//Delete jobTypes
route.delete("/:id", deletejobTypes);

module.exports = route;
