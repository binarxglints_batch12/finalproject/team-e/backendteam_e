const express = require("express");

const route = express.Router();

const {
  getAllindustry_categories,
  getByIdindustry_categories,
  getByNameindustry_categories,
  createindustry_categories,
  updateindustry_categories,
  deleteindustry_categories,
} = require("../controllers/industryCategoriesController");

//Get All industry_categories
route.get("/", getAllindustry_categories);

//Get single industry_categories by id
route.get("/:id", getByIdindustry_categories);

//Get industry_categories by industry_categories Name
route.get("/name/:industry_categories_name", getByNameindustry_categories);

route.post("/", createindustry_categories);

route.put("/:id", updateindustry_categories);

route.delete("/:id", deleteindustry_categories);

module.exports = route;
