const express = require("express");

const route = express.Router();

const {
    getSearchByAll,
    getSearchJobByTitle,
    getPopularJobCategories,
    getFeatureJobByPost,
    getTopCompanies,
} = require("../controllers/homepageController");

//Get All Homepage
// route.get("/", getAllHomepage);

//Get single Homepage by id
// route.get("/:id", getByIdHomepage);

//Get Homepage by Filter Search Name
route.get("/", getSearchByAll);
route.get("/search", getSearchJobByTitle);
route.get("/jobsearch", getPopularJobCategories);
route.get("/jobfeaturesearch", getFeatureJobByPost);
route.get("/topcompaniessearch", getTopCompanies);


module.exports = route;
