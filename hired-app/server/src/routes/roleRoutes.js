const express = require("express");

const route = express.Router();

const { candidate } = require("../middleware/auth");

const {
  getAllRoles,
  getRole,
  createRole,
  updateRole,
  deleteRole,
} = require("../controllers/roleController");

//Get All Roles
route.get("/", getAllRoles);

//Get single Role by id
route.get("/:id", getRole);

//Create Role
route.post("/", createRole);

//Update Role
route.put("/:id", updateRole);

//Delete Role
route.delete("/:id", deleteRole);

module.exports = route;
