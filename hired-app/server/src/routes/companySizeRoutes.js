const express = require("express");

const route = express.Router();

const {
  getAllCompanySize,
  getByIdCompanySize,
  getByNameCompanySize,
  createCompanySize,
  updateCompanySize,
  deleteCompanySize,
} = require("../controllers/companySizeController");

//Get All CompanySize
route.get("/", getAllCompanySize);

//Get single CompanySize by id
route.get("/:id", getByIdCompanySize);

//Get CompanySize by CompanySize Name
route.get("/name/:CompanySize_name", getByNameCompanySize);

route.post("/", createCompanySize);

route.put("/:id", updateCompanySize);

route.delete("/:id", deleteCompanySize);

module.exports = route;
