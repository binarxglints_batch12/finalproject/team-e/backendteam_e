const express = require("express");

const route = express.Router();

const { loginUser, loginEmployee } = require("../controllers/loginController");

//Login as Candidate
route.post("/candidate", loginUser);

//Login as Employee
route.post("/employee", loginEmployee);
module.exports = route;
