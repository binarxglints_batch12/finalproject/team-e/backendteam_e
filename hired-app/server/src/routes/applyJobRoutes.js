const express = require("express");

const route = express.Router();

const { applyJobs } = require("../controllers/applyJobController");

//POST Apply Jobs
route.post("/apply-job", applyJobs);

module.exports = route;
