const express = require("express");

const route = express.Router();

const {
  getAllSalaryRange,
  getSalaryRange,
  createSalaryRange,
  updateSalaryRange,
  deleteSalaryRange,
} = require("../controllers/salaryRangeController");

//Get All SalaryRange
route.get("/", getAllSalaryRange);

//Get single SalaryRange by id
route.get("/:id", getSalaryRange);

//Create SalaryRange
route.post("/", createSalaryRange);

//Update SalaryRange
route.put("/:id", updateSalaryRange);

//Delete SalaryRange
route.delete("/:id", deleteSalaryRange);

module.exports = route;
