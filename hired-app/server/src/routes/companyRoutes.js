const express = require("express");

const route = express.Router();

const {
    getAllCompany,
    getByIdCompany,
    getSearchCompany,
    createCompany,
    updateCompany,
    deleteCompany,
} = require("../controllers/companyController");

//Get All Company
route.get("/", getAllCompany);

//Get single Company by id
// route.get("/:id", getByIdCompany);

//Get Company by Company Name
route.get("/search", getSearchCompany);

route.post("/", createCompany);

route.put("/:id", updateCompany);

route.delete("/:id", deleteCompany);

module.exports = route;
