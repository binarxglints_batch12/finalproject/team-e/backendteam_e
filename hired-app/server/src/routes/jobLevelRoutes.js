const express = require("express");

const route = express.Router();

const {
  getAlljobLevels,
  getjobLevel,
  createjobLevel,
  updatejobLevel,
  deletejobLevel,
} = require("../controllers/jobLevelController");

//Get All jobLevels
route.get("/", getAlljobLevels);

//Get single jobLevel by id
route.get("/:id", getjobLevel);

//Create jobLevel
route.post("/", createjobLevel);

//Update jobLevel
route.put("/:id", updatejobLevel);

//Delete jobLevel
route.delete("/:id", deletejobLevel);

module.exports = route;
