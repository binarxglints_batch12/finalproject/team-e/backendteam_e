const express = require("express");

const route = express.Router();

const {
  getAllSalary_frequency,
  getSalary_frequency,
  createSalary_frequency,
  updateSalary_frequency,
  deleteSalary_frequency,
} = require("../controllers/salaryFrequencyController");

//Get All Salary_frequency
route.get("/", getAllSalary_frequency);

//Get single Salary_frequency by id
route.get("/:id", getSalary_frequency);

//Create Salary_frequency
route.post("/", createSalary_frequency);

//Update Salary_frequency
route.put("/:id", updateSalary_frequency);

//Delete Salary_frequency
route.delete("/:id", deleteSalary_frequency);

module.exports = route;
