const express = require("express");

const route = express.Router();

// import middleware
const auth = require("../middleware/auth");
const { candidate, employee } = require("../middleware/auth");

//import controller
const {
  postJobDetail,
  getJobDetails,
  updateJobDetails,
  deleteJobDetails,
  getJobList,
  getSearchJobs,
} = require("../controllers/jobsDetailcontroller");

//Create jobLevel
route.post("/", employee, postJobDetail);

// Get job details
route.get("/job-details", getJobDetails);

//PUT Update Jobdetails
route.put("/", updateJobDetails);

//DELETE Jobdetails
route.delete("/", deleteJobDetails);

// GET joblists
route.get("/job-lists", getJobList);

// Get Search & Filtering Jobs
route.get("/findJobs", getSearchJobs);

module.exports = route;
