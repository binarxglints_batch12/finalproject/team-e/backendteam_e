const express = require("express");

const route = express.Router();

const {
  getAlljobPostingCategory,
  getjobPostingCategory,
  createjobPostingCategory,
  updatejobPostingCategory,
  deletejobPostingCategory,
} = require("../controllers/jobPostingCategoryController");

//Get All jobPostingCategorys
route.get("/", getAlljobPostingCategory);

//Get single jobPostingCategory by id
route.get("/:id", getjobPostingCategory);

//Create jobPostingCategory
route.post("/", createjobPostingCategory);

//Update jobPostingCategory
route.put("/:id", updatejobPostingCategory);

//Delete jobPostingCategory
route.delete("/:id", deletejobPostingCategory);

module.exports = route;
