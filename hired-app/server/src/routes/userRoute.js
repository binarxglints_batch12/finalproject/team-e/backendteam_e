const express = require("express")
const route = express.Router()
const { candidate } = require("../middleware/auth")

const {
    createUser, updateUser, getAllUsers
}= require('../controllers/userController')

route.post("/register",createUser)
route.put("/:id",updateUser)
route.get('/users',getAllUsers)

module.exports = route