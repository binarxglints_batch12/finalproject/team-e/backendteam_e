const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const { UserModel } = require("../../databases/models/userModel");

const candidate = async (req, res, next) => {
  const token = req.header("Authorization");
  if (!token) {
    return res.status(401).send("Forbidden,no Token");
  }
  try {
    const decoded = jwt.verify(
      req.header("Authorization"),
      process.env.TOKEN_SECRET
    );
    const data = await UserModel.findOne({ _id: decoded.Candidate.id });
    if (!data) {
      return res.status(500).json({
        message: "User Id Not Found!",
      });
    }

    if (data.role !== "Candidate") {
      return res.status(403).json({
        message: "Forbidden,Please Login as Candidate",
      });
    }
    req.user = decoded.user;

    next();
  } catch (error) {
    return res.status(500).json({
      message: error.message,
    });
  }
};

const employee = async (req, res, next) => {
  console.log(req.body);
  const token = req.header("Authorization");
  if (!token) {
    return res.status(401).send("Forbidden,no Token");
  }
  try {
    const decoded = jwt.verify(
      req.header("Authorization"),
      process.env.TOKEN_SECRET
    );
    console.log(decoded);
    const data = await UserModel.findOne({ _id: decoded.Employee.id });
    if (!data) {
      return res.status(500).json({
        message: "Employee Id Not Found!",
      });
    }
    console.log(data);
    if (data.role !== "Employee") {
      return res.status(403).json({
        message: "Forbidden,Please Login as employee",
      });
    }
    req.user = decoded.user;

    next();
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      message: error.message,
    });
  }
};
module.exports = { candidate, employee };
