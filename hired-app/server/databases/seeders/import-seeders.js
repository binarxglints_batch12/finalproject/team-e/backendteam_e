const fs = require("fs");
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const jobsModel = require("../models/jobsModel");
const jobsSchema = require("../models/jobsModel");

dotenv.config();
const dbConnection = async () => {
  try {
    await mongoose.connect(process.env.CONNECTION_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    });

    console.log("Database is Connected.");
  } catch (error) {
    console.log(error);
  }
};

const jobs = JSON.parse(
  fs.readFileSync(`${__dirname}/seeding_jobDetails.JSON`, "utf-8")
);
console.log("HASIL SYNC:", jobs);

const importData = async () => {
  try {
    jobs = await jobsModel.Model.jobs.create(jobs);
    console.log("Data successfully Loaded: ", jobs);
  } catch (error) {
    console.log(error);
  }
};

// DELETE EXISTING DATA
const deleteData = async () => {
  try {
    await jobsModel.deleteMany();
    console.log("Data successfully deleted !");
  } catch (error) {
    console.log(error);
  }
};

if (process.argv[2] === "--import") {
  importData();
} else if (process.argv[2] === "--delete") {
  deleteData();
}
console.log(process.argv);

module.exports = dbConnection;
