const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const jobPostingCategorySchema = new Schema({
  description: {
    type: String,
  },
  jobs: {
    type: Schema.Types.ObjectId,
    ref: "jobs",
  },
});

const jobPostingCategoryModel = mongoose.model(
  "jobPostingCategory",
  jobPostingCategorySchema
);

module.exports = { jobPostingCategoryModel, jobPostingCategorySchema };
