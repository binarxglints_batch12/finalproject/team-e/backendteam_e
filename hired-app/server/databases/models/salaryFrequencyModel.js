const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const salary_frequencySchema = new Schema({
  description: {
    type: String,
  },
  jobs: {
    type: Schema.Types.ObjectId,
    ref: "jobs",
  },
});

const salary_frequencyModel = mongoose.model(
  "salary_frequency",
  salary_frequencySchema
);

module.exports = { salary_frequencyModel, salary_frequencySchema };
