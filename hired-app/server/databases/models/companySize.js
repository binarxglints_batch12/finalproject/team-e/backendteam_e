const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const CompanySizeSchema = new Schema({
  description: {
    type: String,
  },
  Company: {
    type: Schema.Types.ObjectId,
    ref: "company",
  },
});

const CompanySizeModel = mongoose.model("CompanySize", CompanySizeSchema);

module.exports = { CompanySizeModel, CompanySizeSchema };
