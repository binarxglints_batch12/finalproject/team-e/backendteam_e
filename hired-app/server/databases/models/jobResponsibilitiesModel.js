const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const job_responsibilitiesSchema = new Schema({
  description: {
    type: String,
  },
});

const job_responsibilitiesModel = mongoose.model(
  "job_responsibilities",
  job_responsibilitiesSchema
);

module.exports = { job_responsibilitiesModel, job_responsibilitiesSchema };
