const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const applyJobSchema = new Schema({
  jobDetails_id: {
    type: Schema.Types.ObjectId,
    ref: "jobs",
  },
  users_id: {
    type: Schema.Types.ObjectId,
    ref: "User",
  },
  company_id: {
    type: Schema.Types.ObjectId,
    ref: "company",
  },
});

const applyJobModel = mongoose.model("appliedJobs", applyJobSchema);

module.exports = { applyJobModel, applyJobSchema };
