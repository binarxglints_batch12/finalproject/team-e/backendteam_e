const mongoose = require("mongoose");

const { jobLevelSchema } = require("./jobLevelModel");
const { salary_frequencySchema } = require("./salaryFrequencyModel");
const { salary_rangeSchema } = require("./salaryRangeModel");
const { jobTypesSchema } = require("./jobTypesModel");
const { jobPostingCategorySchema } = require("./jobpostingCategory_model");

const Schema = mongoose.Schema;

const jobsSchema = new Schema({
  job_title: {
    type: String,
  },
  description: {
    type: String,
  },
  job_responsibilities: {
    type: String,
  },
  job_requirements: {
    type: String,
  },
  date_Posted: {
    type: Date,
  },
  Job_posting_category: [jobPostingCategorySchema],
  expired_date: {
    type: Date,
  },
  work_hour: {
    type: String,
  },
  salary: {
    type: String,
  },
  job_types: [jobTypesSchema],
  experience_levels: [jobLevelSchema],
  salary_frequency: [salary_frequencySchema],
  salary_range: [salary_rangeSchema],
  isBookmark: {
    type: Boolean,
    default: "false",
  },
  company_id: {
    type: Schema.Types.ObjectId,
    ref: "company",
  },
});

const jobsModel = mongoose.model("jobs", jobsSchema, "jobs");

module.exports = { jobsModel, jobsSchema };
