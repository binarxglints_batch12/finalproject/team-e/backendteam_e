const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const salary_rangeSchema = new Schema({
  description: {
    type: String,
  },
  jobs: {
    type: Schema.Types.ObjectId,
    ref: "jobs",
  },
});

const salary_rangeModel = mongoose.model("salary_range", salary_rangeSchema);

module.exports = { salary_rangeModel, salary_rangeSchema };
