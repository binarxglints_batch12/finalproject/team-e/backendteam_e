const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const industry_categoriesSchema = new Schema({
  description: {
    type: String,
  },
  Company: {
    type: Schema.Types.ObjectId,
    ref: "company",
  },
});

const industry_categoriesModel = mongoose.model(
  "industry_categories",
  industry_categoriesSchema
);

module.exports = { industry_categoriesModel, industry_categoriesSchema };
