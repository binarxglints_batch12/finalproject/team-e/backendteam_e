const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const job_requirementsSchema = new Schema({
  description: {
    type: String,
  },
});

const job_requirementsModel = mongoose.model(
  "job_requirements",
  job_requirementsSchema
);

module.exports = { job_requirementsModel, job_requirementsSchema };
