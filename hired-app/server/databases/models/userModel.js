const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const Schema = mongoose.Schema;

const { roleSchema } = require("../models/roleModel");

const userSchema = new Schema({
  role: {
    type: String,
  },
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    requried: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
    set: encryptedPassword,
  },
  phoneNumber: {
    type: String,
    // required: true,
  },
  birthDay: {
    type: Date,
  },
  address: {
    type: String,
    // required: true
  },
  gender: {
    type: String,
    // required: true
  },
  salaryExpectation: {
    type: Number,
    // required: true
  },
  workEksperience: {
    type: String,
    // required: true
  },
  education: {
    type: String,
    // required: true
  },
  skills: {
    type: String,
    // required: true
  },
  profilePict: {
    type: String,
    get: getprofilepicture,
  },
  fileResume: {
    type: String,
    get: getfileresume,
  },
  saveJobs: {
    type: String,
  },
  applyJobs: {
    type: String,
  },
});

const UserModel = mongoose.model("User", userSchema);

function encryptedPassword(password) {
  const encryptedPassword = bcrypt.hashSync(password, 10);
  return encryptedPassword;
}

function getprofilepicture(profilePict) {
  return `/images/${profilePict}`;
}

function getfileresume(fileResume) {
  return `/file/${fileResume}`;
}

module.exports = { UserModel, userSchema };
