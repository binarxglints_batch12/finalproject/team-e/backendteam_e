const mongoose = require("mongoose");



const Schema = mongoose.Schema;

const { CompanySizeSchema } = require("./companySize");
const { industry_categoriesSchema } = require("./industry_categories");

const CompanySchema = new Schema({
  company_name: {
    type: String,
  },
  company_size: [CompanySizeSchema],

  company_address: {
    type: String,
  },
  company_logo: {
    type: String,
  },
  job_list: {
    type: Schema.Types.ObjectId,
    ref: "jobs",
  },
  company_about: {
    type: String,
  },
  industry_categories: [industry_categoriesSchema],

  location: {
    type: String,
  },

  createdAt:{
    type:Date,
    default: Date.now,

  },
});

const companyModel = mongoose.model("company", CompanySchema, "companies");

module.exports = { companyModel, CompanySchema };
