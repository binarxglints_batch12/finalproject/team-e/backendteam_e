const mongoose = require("mongoose")

const Schema = mongoose.Schema

const roleSchema = new Schema({
    // user_id: {
    //     type: String
    // },
    role_name:{
        type: String
    },
    role_description:{
        type: String
    }
})

const RoleModel = mongoose.model("Role",roleSchema)

module.exports = {RoleModel,roleSchema}