const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const jobTypesSchema = new Schema({
  description: {
    type: String,
  },
  jobs: {
    type: Schema.Types.ObjectId,
    ref: "jobs",
  },
});

const jobTypesModel = mongoose.model("job_types", jobTypesSchema);

module.exports = { jobTypesModel, jobTypesSchema };
