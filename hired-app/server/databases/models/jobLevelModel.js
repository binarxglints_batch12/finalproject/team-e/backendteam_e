const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const jobLevelSchema = new Schema({
  description: {
    type: String,
  },
  jobs: {
    type: Schema.Types.ObjectId,
    ref: "jobs",
  },
});

const jobLevelModel = mongoose.model("jobLevel", jobLevelSchema);

module.exports = { jobLevelModel, jobLevelSchema };
